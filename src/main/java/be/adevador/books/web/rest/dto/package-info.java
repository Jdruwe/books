/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package be.adevador.books.web.rest.dto;
