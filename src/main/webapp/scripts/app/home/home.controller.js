'use strict';

angular.module('booksApp')
    .controller('HomeController', function ($scope, $location, $state, Auth, Principal, ENV, $mdSidenav, menu) {
        $scope.isAuthenticated = Principal.isAuthenticated;
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';

        $scope.logout = function () {
            Auth.logout();
            $state.go('home');
        };

        $scope.toggleSidenav = function (menuId) {
            $mdSidenav(menuId).toggle();
        };

        $scope.vm = this;

        //functions for menu-link and menu-toggle
        $scope.vm.isOpen =  function isOpen(section) {
            return menu.isSectionSelected(section);
        };

        $scope.vm.toggleOpen = function toggleOpen(section) {
            menu.toggleSelectSection(section);
        };

        $scope.vm.autoFocusContent = false;
        $scope.vm.menu = menu;

        $scope.vm.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };
    });
