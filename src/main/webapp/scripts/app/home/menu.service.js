(function () {
    'use strict';
    angular.module('booksApp')
        .factory('menu', [
            '$location',
            '$rootScope',
            function ($location) {

                var sections = [{
                    name: 'Home',
                    state: 'home',
                    type: 'link'
                }];

                sections.push({
                    name: 'Account',
                    type: 'toggle',
                    pages: [{
                        name: 'Settings',
                        type: 'link',
                        state: 'login',
                        icon: 'fa fa-group'
                    }, {
                        name: 'Password',
                        state: 'home.beers.porters',
                        type: 'link',
                        icon: 'fa fa-map-marker'
                    }, {
                        name: 'Sign Out',
                        state: 'home.beers.wheat',
                        type: 'link',
                        icon: 'fa fa-plus'
                    }]
                });

                sections.push({
                    name: 'Account',
                    type: 'toggle',
                    pages: [{
                        name: 'Settings',
                        type: 'link',
                        state: 'login',
                        icon: 'fa fa-group'
                    }, {
                        name: 'Password',
                        state: 'home.beers.porters',
                        type: 'link',
                        icon: 'fa fa-map-marker'
                    }, {
                        name: 'Sign Out',
                        state: 'home.beers.wheat',
                        type: 'link',
                        icon: 'fa fa-plus'
                    }]
                });

                var self;

                return self = {
                    sections: sections,

                    toggleSelectSection: function (section) {
                        self.openedSection = (self.openedSection === section ? null : section);
                    },
                    isSectionSelected: function (section) {
                        return self.openedSection === section;
                    },

                    selectPage: function (section, page) {
                        page && page.url && $location.path(page.url);
                        self.currentSection = section;
                        self.currentPage = page;
                    }
                };

                function sortByHumanName(a, b) {
                    return (a.humanName < b.humanName) ? -1 :
                        (a.humanName > b.humanName) ? 1 : 0;
                }

            }]);

})();
