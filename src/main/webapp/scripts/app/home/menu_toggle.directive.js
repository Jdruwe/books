angular.module('booksApp')
    .run(['$templateCache', function ($templateCache) {
        $templateCache.put('partials/menu-toggle.tmpl.html', '<md-button class="md-button-toggle" ng-class="{\'toggled\' : isOpen()}" ng-click="toggle()" aria-controls="docs-menu-{{section.name | nospace}}" flex layout="row" aria-expanded="{{isOpen()}}">{{section.name}}<md-icon md-font-icon="fa fa-chevron-down" class="md-toggle-icon" ng-class="{\'toggled\' : isOpen()}"></md-icon></md-button><ul ng-show="isOpen()" id="docs-menu-{{section.name | nospace}}" class="menu-toggle-list"><li ng-repeat="page in section.pages"><menu-link section="page"></menu-link></li></ul>');
    }])
    .directive('menuToggle', function () {
        return {
            scope: {
                section: '='
            },
            templateUrl: 'scripts/app/home/partial/menu-toggle.html',
            link: function (scope, element) {
                var controller = element.parent().controller();

                scope.isOpen = function () {
                    return controller.isOpen(scope.section);
                };
                scope.toggle = function () {
                    controller.toggleOpen(scope.section);
                };

                var parentNode = element[0].parentNode.parentNode.parentNode;
                if (parentNode.classList.contains('parent-list-item')) {
                    var heading = parentNode.querySelector('h2');
                    element[0].firstChild.setAttribute('aria-describedby', heading.id);
                }
            }
        };
    });
