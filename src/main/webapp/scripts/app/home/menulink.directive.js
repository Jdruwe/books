'use strict';

angular.module('booksApp')
    .run(['$templateCache', function ($templateCache) {
        //$templateCache.put('partials/menu-link.tmpl.html', '<md-button ng-class="{\'{{section.icon}}\' : true}" ui-sref-active="active" ui-sref="{{section.state}}" ng-click="focusSection()"> {{section | humanizeDoc}} <span class="md-visually-hidden " ng-if="isSelected()">current page</span></md-button>');
        $templateCache.put('partials/menu-link.tmpl.html', '<md-button ui-sref-active="active" ui-sref="{{section.state}}" ng-click="focusSection()"> {{section | humanizeDoc}} <span class="md-visually-hidden " ng-if="isSelected()">current page</span></md-button>');
    }])
    .directive('menuLink', function () {
        return {
            scope: {
                section: '='
            },
            templateUrl: 'partials/menu-link.tmpl.html',
            link: function ($scope, $element) {
                var controller = $element.parent().controller();

                $scope.focusSection = function () {
                    // set flag to be used later when
                    // $locationChangeSuccess calls openPage()
                    controller.autoFocusContent = true;
                };
            }
        };
    });

