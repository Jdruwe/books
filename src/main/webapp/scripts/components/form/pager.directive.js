/* globals $ */
'use strict';

angular.module('booksApp')
    .directive('booksAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
