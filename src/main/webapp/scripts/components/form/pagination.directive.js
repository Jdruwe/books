/* globals $ */
'use strict';

angular.module('booksApp')
    .directive('booksAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
